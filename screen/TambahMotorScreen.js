import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  Keyboard,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { supabase } from '../components/Supabase';
 
const TambahKendaraan = ({navigation}) => {
  const [namaKendaraan, setNameKendaraan] = useState("");
  const [nomorPlat, setNomorPlat] = useState("");
  const [merek, setMerek] = useState("");
  const [tipe, setTipe] = useState("");
  const [warna, setWarna] = useState("");

  const handleTambahMotor = async () => {
    Keyboard.dismiss();

    const { data, error } = await supabase.auth.getSession();

    let { data:pengguna, error2 } = await supabase
    .from('pengguna')
    .select()
    .like('email_pengguna', `%${data.session.user.email}%`);


    const {data:kendaraan, error3} = await supabase
    .from('kendaraan')
    .insert([
      { 
        nama_kendaraan: namaKendaraan, 
        nomor_kendaraan: nomorPlat,
        color: warna,
        brand: merek,
        type: tipe,
        pemilik: pengguna[0].id,
      },
    ])

    if(!error){
      alert("Motor berhasil ditambahkan");
      navigation.navigate('LandingUser');
    }
  }

  return (
    <ScrollView style={styles.container}>
      <StatusBar style="auto" />

      <View style={styles.headerView} >
        <Text style={styles.title}>Tambah kendaraan</Text>
      </View>

      <Image
        style={styles.image}
        source={require('../images/motor.png')}
      />

      <View style={{alignSelf: 'center', marginTop: 8}}>
        <TouchableOpacity style={styles.editFoto}>
        <Image
                style={styles.icon}
                source={require('../images/icon/border_color_icon.png')}
            />
            <Text style={styles.editFotoText}>Edit foto</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.formView}>
        <View style={styles.inputView}>
          <Text style={styles.inputLabel}>Nama Kendaraan</Text>
          <TextInput
            style={styles.TextInput}
            placeholder="ex. Motor Doe"
            placeholderTextColor="#BABABB"
            keyboardType="email-address"
            returnKeyType="next"
            onChangeText={(namaKendaraan) => setNameKendaraan(namaKendaraan)}
          />
        </View>

        <View style={styles.inputView}>
          <Text style={styles.inputLabel}>Nomor Plat</Text>
          <TextInput
            style={styles.TextInput}
            placeholder="ex. B 1234 ABC"
            placeholderTextColor="#BABABB"
            keyboardType="email-address"
            returnKeyType="next"
            onChangeText={(nomorPlat) => setNomorPlat(nomorPlat)}
          />
        </View>

        <View style={styles.inputView}>
          <Text style={styles.inputLabel}>Merek</Text>
          <TextInput
            style={styles.TextInput}
            placeholder="ex. Yamaha"
            placeholderTextColor="#BABABB"
            keyboardType="email-address"
            returnKeyType="next"
            onChangeText={(merek) => setMerek(merek)}
          />
        </View>

        <View style={styles.inputView}>
          <Text style={styles.inputLabel}>Tipe</Text>
          <TextInput
            style={styles.TextInput}
            placeholder="ex. Aerox"
            placeholderTextColor="#BABABB"
            keyboardType="email-address"
            returnKeyType="next"
            onChangeText={(tipe) => setTipe(tipe)}
          />
        </View> 

        <View style={styles.inputView}>
          <Text style={styles.inputLabel}>Warna</Text>
          <TextInput
            style={styles.TextInput}
            placeholder="ex. Kuning"
            placeholderTextColor="#BABABB"
            keyboardType="email-address"
            returnKeyType="next"
            onChangeText={(warna) => setWarna(warna)}
          />
        </View> 
        <TouchableOpacity 
          style={styles.tambahBtn}
          onPress={() => handleTambahMotor()}>
          <Text style={styles.tambahText}>Tambah</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

export default TambahKendaraan

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    paddingTop: 80,
    width: "100%",
    height: "100%",
    backgroundColor: "#F9FAFF",
    paddingHorizontal: 20,
  },
 
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    alignSelf: 'center', 
    color: "#3D4058",
  },

  image:{
    width: 100,
    height: 100,
    alignSelf: 'center',
    borderRadius: 16,
  },

  editFoto:{
    flexDirection: 'row',
    backgroundColor : '#EEEEFF',
    padding: 8,
    borderRadius: 8,
    
  },

  editFotoText:{
    color: '#3D4058',
    fontSize: 12,
  },

  headerView: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    justifyContent: 'flex-end',
    paddingBottom: 15,
  },

  formView: {
    width: "100%",
    flex: 6,
  },

  inputView: {
    flexDirection: 'column',
    height: 80,
    marginTop: 20,
  },

  inputLabel: {
    fontWeight: 'bold',
    fontSize: 14,
    flex: 2,
    color: '#3D4058',
  },

  TextInput: {
    alignItems: 'stretch',
    flex: 3,
    color: 'black',
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 12,
    borderColor: '#dadae8',
  },
 
  tambahBtn: {
    width: "100%",
    borderRadius: 12,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: "#EF233C",
    marginBottom: 150
  },

  tambahText: {
    color: '#fff',
  },

  RedText:{
    color:"#EF233C",
  },

  icon:{
    width: 15,
    height: 15,
    marginRight: 4,
  }
});

