import React, {useState, useEffect} from 'react';
import { StatusBar } from 'expo-status-bar';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import { supabase } from '../components/Supabase';
 
const LoginScreen = ({ navigation }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errortext, setErrortext] = useState('');

  const handleSubmitPress = async () => {
    Keyboard.dismiss();
    setErrortext('');
    if (!email) {
      alert('Please fill Email');
      return;
    }
    if (!password) {
      alert('Please fill Password');
      return;
    }

    // console.log(email);
    // console.log(password);

    const { data, error } = await supabase.auth.signInWithPassword({
      email: email,
      password: password,
    })

    let { data:pengguna, error2 } = await supabase
    .from('pengguna')
    .select()
    .like('email_pengguna', `%${email}%`);

    console.log(pengguna);

    let { data:merchant, error3 } = await supabase
    .from('merchant')
    .select()
    .like('email_merchant', `%${data.session.user.email}%`);

    if (error) {
      alert("There is an error with your login process!");
      return;
    }else if (pengguna.length != 0){
      alert("You have been Logged In Succesfully");
      navigation.navigate('LandingUser');
    }else if(merchant.length != 0){
      navigation.navigate('DashboardMerchant')
    }
  }

  async function LoginCheck(){
    const { data, error } = await supabase.auth.getSession();
    // Uncomment for debug
    // console.log(data.session.user.email);
    let { data:pengguna, error2 } = await supabase
    .from('pengguna')
    .select()
    .like('email_pengguna', `%${data.session.user.email}%`);

    let { data:merchant, error3 } = await supabase
    .from('merchant')
    .select()
    .like('email_merchant', `%${data.session.user.email}%`);
    
    // console.log(pengguna);
    if(error){
        navigation.navigate('Login');
    }
    else if(pengguna.length != 0){
      navigation.navigate('LandingUser');
    }else if(merchant.length != 0){
      console.log("masuk sini");
      navigation.navigate('DashboardMerchant')
    }
  }

  useEffect(()=> {
      async function fetchData() {
          let data = await LoginCheck();

          // Uncomment for debug
          // console.log(data);
      }
      fetchData();
  },[])
 
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />

      <View style={styles.headerView} >
        <Image style={styles.image} source={require("../images/login.png")} />
        <Text style={styles.LeftTitle}>Login</Text>
      </View>

      <View style={styles.formView}>
        <View style={styles.inputView}>
          <Text style={styles.InputCaption}>Email</Text>
          <TextInput
            style={styles.TextInput}
            placeholder="Masukkan Email"
            placeholderTextColor="#BABABB"
            onChangeText={(email) => setEmail(email)}
            keyboardType="email-address"
            returnKeyType="next"
          />
        </View>
  
        <View style={styles.inputView}>
          <Text style={styles.InputCaption}>Password</Text>
          <TextInput
            style={styles.TextInput}
            placeholder="Masukkan Password"
            placeholderTextColor="#BABABB"
            secureTextEntry={true}
            onChangeText={(password) => setPassword(password)}
          />
        </View>

        {errortext != '' ? (
              <Text style={styles.errorTextStyle}>
                {errortext}
              </Text>
            ) : null}

        <TouchableOpacity 
          style={styles.loginBtn}
          onPress={handleSubmitPress}>
          <Text style={styles.loginText}>Login</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.forgot_button} onPress={() => navigation.navigate('RegisNav')}>
          <Text >Belum memiliki akun? <Text style={{color: "#EF233C"}}>Daftar sekarang</Text></Text>
        </TouchableOpacity>
      </View>

    </View>
  );
}

export default LoginScreen
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: "100%",
    backgroundColor: "#F9FAFF",
    justifyContent: "center",
    paddingHorizontal: 30,
  },
 
  image: {
    width: 250,
    height: 250,
    resizeMode: 'contain',
    margin: 10,
  },

  LeftTitle: {
    fontWeight: 'bold',
    fontSize: 24,
    alignSelf: 'flex-start', 
    color: '#3D4058'
  },

  headerView: {
    width: "100%",
    flex: 2,
    alignItems: "center",
    justifyContent: 'flex-end',
  },

  formView: {
    width: "100%",
    flex: 3,
  },

  inputView: {
    flexDirection: 'column',
    height: 80,
    marginTop: 20,
  },

  ScreenTitle: {
    fontWeight: 'bold',
    fontSize: 24,
  },

  InputCaption: {
    fontWeight: 'bold',
    fontSize: 14,
    flex: 2,
    color: '#3D4058',
  },

  TextInput: {
    alignItems: 'stretch',
    flex: 3,
    color: 'black',
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 12,
    borderColor: '#dadae8',
  },
 
  forgot_button: {
    height: 30,
    marginTop: 10,
    alignItems:'center',
    color: '#3D4058',
  },
 
  loginBtn: {
    width: "100%",
    borderRadius: 12,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: "#EF233C",
  },

  loginText: {
    color: '#fff',
  },
});