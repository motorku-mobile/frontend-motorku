import React, {useEffect, useState} from 'react';
import { StatusBar } from 'expo-status-bar';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import { supabase } from '../components/Supabase';
import { ScrollView } from 'react-native-gesture-handler';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import * as Location from 'expo-location';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';

const SearchScreen = ({ navigation }) => {

    const [locationNow, setLocationNow] = useState("not set");
    const [latitude, setLatitude] = useState(0);
    const [longitude, setLongitude] = useState(0);
    const [merchantCard, setMerchantCard] = useState([]);
    const [searchResultCard, setSearchResultCard] = useState([]);
    const [mapMarker, setMapMarker] = useState([]);
    const [nearbyMapMarker, setNearbyMapMarker] = useState([]);

    async function getLatLongNow (){
        let { status } = await Location.requestForegroundPermissionsAsync();
        if (status !== 'granted') {
        console.log('Permission to access location was denied');
        return;
        }

        let location = await Location.getCurrentPositionAsync({});
        // console.log(location);
        setLatitude(location['coords']['latitude']);
        setLongitude(location['coords']['longitude']);
    };

    async function handleMap(event){
        let { data: merchants, error } = await supabase
        .from('merchant')
        .select('*');

        setSearchResultCard([<Text style={{ fontWeight: 'bold', paddingBottom: 10, color: "#3D4058"}}>Hasil Pencarian</Text>]);
        setNearbyMapMarker([]);

        merchants.forEach((data) => {
            // console.log(data["id"]);
            if(Math.abs(event.latitude - data.latitude + event.longitude - data.longitude) <= 0.01){
                setSearchResultCard( merchantCard => [...merchantCard,            
                    <TouchableOpacity style={styles.mercCard} onPress={() => navigation.navigate('MercDetail', {merchant_id:data["id"]})}>
                    <View>
                        <Image style={styles.merchThumb} source={require("../assets/icon.png")} />
                    </View>
                    <View style={{ paddingHorizontal: 20, justifyContent: 'space-between', width: '70%' }}>
                        <Text style={{ fontSize: 12, fontWeight: 'bold', color: "#3D4058" }}>{data["nama_merchant"]}</Text>
                        <Text style={{ fontSize: 10, color: '#8D99AE' }}>{data["alamat"]}</Text>
                        <View style={{ flexDirection: 'row'}} >
                            <Text style={{ fontSize: 8, color: '#8D99AE' }}>{data["jam_buka"]}-{data["jam_tutup"]}</Text>
                        </View>
                    </View>
                    <View style={{ justifyContent: 'center' }} >
                        <Image style={styles.icon} source={require("../images/icon/chevron_right_icon.png")}/>
                    </View>
                </TouchableOpacity>]);

                setNearbyMapMarker( mapMarker => [...mapMarker,
                    <Marker
                    coordinate={{ latitude : parseFloat(data.latitude) , longitude : parseFloat(data.longitude) }}
                    key={data.id}
                    title={data.nama_merchant}
                    pinColor={'green'}
                />
                ]);
            };
        })
    }

    async function handleSearch(event){
        let { data: merchants, error } = await supabase
        .from('merchant')
        .select('*');

        setSearchResultCard([<Text style={{ fontWeight: 'bold', paddingBottom: 10, color: "#3D4058"}}>Hasil Pencarian</Text>]);
        setMapMarker([]);

        merchants.forEach((data) => {
            let searchKey = data["alamat"] + " " + data["nama_merchant"];
            
            // console.log(searchKey);
            if(searchKey.includes(event)){
                setSearchResultCard( merchantCard => [...merchantCard,            
                    <TouchableOpacity style={styles.mercCard} onPress={() => navigation.navigate('MercDetail', {merchant_id:data["id"]})}>
                    <View>
                        <Image style={styles.merchThumb} source={require("../assets/icon.png")} />
                    </View>
                    <View style={{ paddingHorizontal: 20, justifyContent: 'space-between', width: '70%' }}>
                        <Text style={{ fontSize: 12, fontWeight: 'bold', color: "#3D4058" }}>{data["nama_merchant"]}</Text>
                        <Text style={{ fontSize: 10, color: '#8D99AE' }}>{data["alamat"]}</Text>
                        <View style={{ flexDirection: 'row'}} >
                            <Text style={{ fontSize: 8, color: '#8D99AE' }}>{data["jam_buka"]}-{data["jam_tutup"]}</Text>
                        </View>
                    </View>
                    <View style={{ justifyContent: 'center' }} >
                        <Image style={styles.icon} source={require("../images/icon/chevron_right_icon.png")}/>
                    </View>
                </TouchableOpacity>]);
                
                setMapMarker( mapMarker => [...mapMarker,
                    <Marker
                    coordinate={{ latitude : parseFloat(data.latitude) , longitude : parseFloat(data.longitude) }}
                    key={data.id}
                    title={data.nama_merchant}
                    pinColor={'blue'}
                    style={{width:250}}
                />
                ]);
            }
        })

        console.log(mapMarker)
    }
    
    async function getNearMerchant(){        
        var merchantCardNodes;
        let { data: merchants, error } = await supabase
        .from('merchant')
        .select('*');

        if(!error){
            merchants.forEach(async(data) => {
                let response = await fetch(`https://maps.googleapis.com/maps/api/distancematrix/json?destinations=${data["latitude"]} ${data["longitude"]}&origins=${latitude} ${longitude}&key=AIzaSyCyar_JNDkaRnBlmWnKos2t4xosHF-wKac`);
                response = await response.json();
                if(response['rows'][0]['elements'][0]['status'] == "OK"){
                    let distance_text = response['rows'][0]['elements'][0]['distance']['text'];
                    let distance_val = response['rows'][0]['elements'][0]['distance']['value'];

                    if (distance_val <= 10000){
                        setMerchantCard( merchantCard => [...merchantCard,            
                            <TouchableOpacity style={styles.mercCard} onPress={() => navigation.navigate('MercDetail', {merchant_id:data["id"]})}>
                            <View>
                                <Image style={styles.merchThumb} source={require("../assets/icon.png")} />
                            </View>
                            <View style={{ paddingHorizontal: 20, justifyContent: 'space-between', width: '70%' }}>
                                <Text style={{ fontSize: 12, fontWeight: 'bold' }}>{data["nama_merchant"]}</Text>
                                <Text style={{ fontSize: 10, color: '#8D99AE' }}>{data["alamat"]}</Text>
                                <View style={{ flexDirection: 'row'}} >
                                    <Text style={{ fontSize: 8, color: '#8D99AE' }}>{distance_text}</Text>
                                    <Text style={{ fontSize: 8, color: '#8D99AE' }}>{data["jam_buka"]}-{data["jam_tutup"]}</Text>
                                </View>
                            </View>
                            <View style={{ justifyContent: 'center' }} >
                                <FontAwesome name={'chevron-right'} color={'#C4C4C4'} size={12} />
                            </View>
                        </TouchableOpacity>]);
                        setNearbyMapMarker( mapMarker => [...mapMarker,
                            <Marker
                            coordinate={{ latitude : parseFloat(data.latitude) , longitude : parseFloat(data.longitude) }}
                            key={data.id}
                            title={data.nama_merchant}
                            pinColor={'green'}
                        />
                        ]);
                    }
                }
            })
        }
    }

    async function getLocationNow(){
        if(latitude && longitude){
            let response = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCyar_JNDkaRnBlmWnKos2t4xosHF-wKac&latlng=${latitude},${longitude}`);
            response = await response.json();
            // console.log("masuk");
            setLocationNow(response["results"][0]["formatted_address"]);
        }
    }

    // CHECK LOGIN BLOCK
    async function LoginCheck(){
        const { data, error } = await supabase.auth.getSession();
        // Uncomment for debug
        // console.log(data);

        if(error){
            navigation.navigate('Login');
        }else{
            return data;
        }
    };

    useEffect(()=> {
        setMerchantCard([]);
        async function fetchData() {
            let data = await LoginCheck();
            
            // Uncomment for debug
            // console.log(data);
        }
        fetchData();
    },[]);
    // END CHECK LOGIN BLOCK

    useEffect(() => {
        async function fetchData() {
            getLatLongNow();
            getLocationNow();
            await getNearMerchant();
        }
        fetchData();
    }, [latitude, longitude]);

    // console.log(latitude);
    // console.log(longitude);

    return (
        <ScrollView style={styles.container}>
            <StatusBar style="auto" />
            
            <View style={styles.headerView} >
                <Text style={styles.title}>Lokasi</Text>
            </View>

            <View style={styles.locView} >
            <Image style={{width: 25, height: 25, marginRight: 8}} source={require("../images/icon/my_location_icon.png")} />
                <View>
                    <Text style={{ fontWeight: 'bold', color: "#3D4058"}}>Lokasi terkini </Text>
                    <Text style={{ color: "#8D99AE"}}>{locationNow}</Text>
                </View>
            </View>

            <View style={styles.formView}>
                <View style={styles.inputView}>
                    <TextInput
                        style={styles.TextInput}
                        placeholder="Cari Lokasi"
                        placeholderTextColor="#BABABB"
                        onChangeText={handleSearch}
                    />
                </View>
            </View>
            <View style={styles.mapView}>
                <MapView
                style={styles.map}
                provider={PROVIDER_GOOGLE}
                region={{
                    latitude: latitude,
                    longitude: longitude,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                  }}>
                <Marker draggable
                    coordinate={{ latitude : latitude , longitude : longitude }}
                    key={1}
                    title={'Position Now'}
                    style={{width:250}}
                    onDragEnd={(e) => {
                        handleMap(e.nativeEvent.coordinate);
                    }}
                />
                {mapMarker.map(x => x)}
                {nearbyMapMarker.map(x => x)}
                </MapView>
            </View>

            <View style={styles.mercView}>
                {searchResultCard.map(x => x)}
            </View>

            <View style={styles.mercView}>
                <Text style={{ fontWeight: 'bold', paddingBottom: 10, }}>Merchant Terdekat</Text>
                {merchantCard.map(x => x)}
            </View>

        </ScrollView>
        
    )
}

export default SearchScreen

const styles = StyleSheet.create({
    container: {
        width: "100%",
        height: "100%",
        backgroundColor: "#F9FAFF",
        paddingHorizontal: 20,
    },

    title: {
        fontWeight: 'bold',
        fontSize: 20,
        alignSelf: 'center', 
        color:'#3D4058'
    },

    headerView: {
        marginTop: 60,
        marginBottom:20,
        width: "100%",
        alignItems: "center",
        justifyContent: 'center',
    },

    locView: {
        width: "100%",
        flexDirection: 'row',
        alignItems: "center",
    },

    formView: {
        width: "100%",
    },

    inputView: {
        flexDirection: 'column',
        height: 50,
        marginTop: 20,
    },

    TextInput: {
        alignItems: 'stretch',
        flex: 1,
        color: 'black',
        paddingLeft: 15,
        paddingRight: 15,
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#dadae8',
    },

    mapView: {
        marginVertical: 25, 
        flex: 3,
        height: 400,
        borderRadius: 16,
    },

    mercView: {
        flex: 1,
    },

    mercCard: {
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#dadae8',
        paddingVertical: 12,
        paddingHorizontal: 20,
        marginVertical: 5,
    },

    merchThumb: {
        height: 50,
        width: 80,
        borderRadius: 6,
    },

    moreIcon: {
        height: 12,
        width: 8,
    },

    map: {
        width: '100%',
        height: '100%', 
        borderRadius: 16,
    },

    icon:{
        width: 30,
        height: 30,
    },
     
});