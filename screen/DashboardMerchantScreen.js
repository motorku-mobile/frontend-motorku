import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { supabase } from '../components/Supabase';
 
const ListPesananScreen = ({navigation}) => {
  const [profileMerchant, setProfileMerchant] = useState();

  async function LogOut(){
    let { error } = await supabase.auth.signOut()

    if(!error){
      navigation.navigate('Login')
    }else{
      alert(error);
    }
  }

  async function LoginCheck(){
    const { data, error } = await supabase.auth.getSession();
    // Uncomment for debug
    // console.log(data.session.user.email);
    let { data:pengguna, error2 } = await supabase
    .from('pengguna')
    .select()
    .like('email_pengguna', `%${data.session.user.email}%`);
  
    let { data:merchant, error3 } = await supabase
    .from('merchant')
    .select()
    .like('email_merchant', `%${data.session.user.email}%`);
    
    // console.log(pengguna);
    if(error){
        navigation.navigate('Login');
    }
    else if(pengguna.length != 0){
      navigation.navigate('LandingUser');
    }else if(merchant.length != 0){
      navigation.navigate('DashboardMerchant')
      setProfileMerchant(merchant);
    }
  }
  
  useEffect(()=> {
      async function fetchData() {
          await LoginCheck();
  
          // Uncomment for debug
          // console.log(data);
          // setProfileMerchant(data);
      }
      fetchData();
  },[])

  return (
    <ScrollView style={styles.container}>
      <StatusBar style="auto" />

      <View style={styles.headerTopView} >
        <Text style={styles.title}>Dasboard</Text>
        <TouchableOpacity onPress={() => LogOut()}>
          <Text style={styles.logout}>Logout</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.merchantCard}>
          <Image style={styles.backgroundMerchant} source={require("../images/backgroundmerchant.png")} />
          <View>
            <Image style={styles.motorImage} source={require("../images/motor.png")} />
          </View>
          <View>
                <Text style={styles.merchantTitle}>{profileMerchant? profileMerchant[0].nama_merchant: ""}</Text>
                <Text style={styles.merchantSubtitle}>{profileMerchant? profileMerchant[0].alamat: ""}</Text>
                <Text style={styles.merchantSubtitle}>{profileMerchant? profileMerchant[0].jam_buka: ""} - {profileMerchant? profileMerchant[0].jam_tutup: ""}</Text>
          </View>
      </View>

      <View style={styles.headerView} >
        <Text style={styles.subTitle}>Order(s)</Text>
      </View>

      <TouchableOpacity>
      <View style={styles.orderCard}>
        <View style={styles.orderContainer}>
            <View style={styles.orderDetail}>
              <View style={styles.order}>
                <Text style={styles.orderName}>Order <Text style={styles.orderId}>#1 </Text></Text>
                <View style={{backgroundColor: '#D82C0D', paddingHorizontal: 4, alignSelf: 'center', borderRadius: 4, marginHorizontal: 4,}}>
                  <Text style={{color: '#ffffff', fontSize: 12}}>New</Text>
                </View>
                </View>
                <Text style={styles.orderPrice}>Rp 10000 - 3 Items</Text>
                <Text style={styles.orderPrice}>Swadarma Utara 1 No. 39 RT 009 RW 08 Ulujami Pesanggrahan Jakarta Selatan</Text>
            </View>
        </View>
        <View style={{alignSelf: 'center', marginHorizontal: 4}}>
            <Image
                style={styles.icon}
                source={require('../images/icon/chevron_right_icon.png')}
            />
        </View>
    </View>
      </TouchableOpacity>

      <TouchableOpacity>
      <View style={styles.orderCard}>
        <View style={styles.orderContainer}>
            <View style={styles.orderDetail}>
              <View style={styles.order}>
                <Text style={styles.orderName}>Order <Text style={styles.orderId}>#1 </Text></Text>
                <View style={{backgroundColor: '#FEB62E', paddingHorizontal: 4, alignSelf: 'center', borderRadius: 4, marginHorizontal: 4,}}>
                  <Text style={{color: '#ffffff', fontSize: 12}}>In progress</Text>
                </View>
                </View>
                <Text style={styles.orderPrice}>Rp 10000 - 3 Items</Text>
                <Text style={styles.orderPrice}>Swadarma Utara 1 No. 39 RT 009 RW 08 Ulujami Pesanggrahan Jakarta Selatan</Text>
            </View>
        </View>
        <View style={{alignSelf: 'center', marginHorizontal: 4}}>
            <Image
                style={styles.icon}
                source={require('../images/icon/chevron_right_icon.png')}
            />
        </View>
    </View>
      </TouchableOpacity>

      <TouchableOpacity>
      <View style={styles.orderCard}>
        <View style={styles.orderContainer}>
            <View style={styles.orderDetail}>
              <View style={styles.order}>
                <Text style={styles.orderName}>Order <Text style={styles.orderId}>#1 </Text></Text>
                <View style={{backgroundColor: '#008060', paddingHorizontal: 4, alignSelf: 'center', borderRadius: 4, marginHorizontal: 4,}}>
                  <Text style={{color: '#ffffff', fontSize: 12}}>Complete</Text>
                </View>
                </View>
                <Text style={styles.orderPrice}>Rp 10000 - 3 Items</Text>
                <Text style={styles.orderPrice}>Swadarma Utara 1 No. 39 RT 009 RW 08 Ulujami Pesanggrahan Jakarta Selatan</Text>
            </View>
        </View>
        <View style={{alignSelf: 'center', marginHorizontal: 4}}>
            <Image
                style={styles.icon}
                source={require('../images/icon/chevron_right_icon.png')}
            />
        </View>
    </View>
      </TouchableOpacity>


    </ScrollView>
  );
}

export default ListPesananScreen

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    paddingTop: 80,
    width: "100%",
    height: "100%",
    backgroundColor: "#F9FAFF",
    paddingHorizontal: 20,
  },
 
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    alignSelf: 'flex-start', 
    color: "#3D4058",
  },

  headerView: {
    width: "100%",
    flex: 1,
    paddingBottom: 15,
  },

  headerTopView:{
    width: "100%",
    flex: 1,
    alignItems: "center",
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingBottom: 15,
  },

orderContainer: {
    flexDirection: 'row',
    marginVertical: 5,
},

orderCard: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#ffffff',
    paddingHorizontal: 8,
    paddingVertical: 12,
    marginVertical: 4,
    borderWidth: 0.5,
    borderRadius: 8,
    borderColor: '#E8E8E8',
},

order:{
  flexDirection: 'row',
},

logout:{
  fontWeight: 'regular',
  fontSize: 12,
  color: "#EF233C",
},

motorImage: {
    height: 90,
    width: 90,
    borderRadius: 60,
    marginHorizontal: 8,
    alignSelf:'center',
},

orderName:{
  fontSize: 14,
  fontWeight: 'bold',
  color: '#3D4058',
  marginBottom: 4,
},

orderId:{
  color: '#8D99AE',
  fontWeight: 'bold',
},

orderPrice:{
  fontSize: 12,
  color: '#8D99AE',
  marginVertical: 4,
},

orderDetail:{
    paddingHorizontal: 8, 
    height: 70,
    justifyContent: 'space-evenly'
},

backgroundMerchant:{
  position: 'absolute',
  width: 372,
  height: 205,
  borderRadius: 8,
  alignContent: 'center',
  top: 0,
  bottom: 0,
  left: 0,
  right: 0,
},
merchantCard:{
  padding: 16,
  borderRadius: 8,
  marginBottom: 16,
},

merchantTitle:{
    fontSize: 16, 
    fontWeight: 'bold', 
    color: "#FFFFFF",
    marginVertical: 4,
    alignSelf: 'center',
},

merchantSubtitle:{
    fontSize: 14, 
    color: '#FFFFFF',
    alignSelf: 'center',
    textAlign: 'center',
},

subheaderView: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    paddingTop: 24,
    paddingBottom: 16,
},

subTitle:{
    fontWeight: 'bold',
    fontSize: 16,
    alignSelf: 'flex-start', 
    color: "#3D4058",
},

  nextText: {
    color: '#fff',
  },

  icon:{
    width: 30,
    height: 30,
  },

});

