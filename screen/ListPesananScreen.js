import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
 
const ListPesananScreen = ({route,navigation}) => {
    var listPesanan = route.params.orderList;
    var listBarang = route.params.itemList;
    const [listPesananCard, setListPesananCard] = useState([]);
    const [totalPrice, setTotalPrice] = useState(0);
    const [totalQuantity, setTotalQuantity] = useState(0);

    function handleOrderList(){
        listBarang.forEach(async x => {
            if(listPesanan[x.id]){
                var totalPriceNodes = parseInt(x.price) * parseInt(listPesanan[x.id])
                var quantityNodes = listPesanan[x.id]
                setTotalPrice(totalPrice + totalPriceNodes);
                setTotalQuantity(totalQuantity + quantityNodes);
                setListPesananCard([...listPesananCard,
                    <View style={styles.productCard}>
                        <View style={styles.productContainer}>
                            <View>
                                <Image style={styles.productImage} source={require("../images/yamalube.jpg")} />
                            </View>
                            <View style={styles.productDetail}>
                                <Text style={styles.productName}>{x.name}</Text>
                                <Text style={styles.productPrice}>Rp {totalPriceNodes}</Text>
                            </View>
                        </View>
                        <View style={{alignSelf: 'center'}}>
                            <Text style={styles.productPrice}>Qty : {listPesanan[x.id]}</Text>
                        </View>
                    </View>
                ])
            }
        })
    }

    useState(() => {
        async function fetchData(){
            await handleOrderList();
        }
        fetchData();
    },[])
  return (
    <ScrollView style={styles.container}>
      <StatusBar style="auto" />

      <View style={styles.headerView} >
        <Text style={styles.title}>List Pesanan</Text>
      </View>

        <View style={styles.info}>
            <FontAwesome name={'info'} color={'#848EDE'} style={{alignSelf: 'center', marginLeft:8, marginRight:0}} size={16}/>
            <Text style={styles.infoText}>Pastikan pesanan kamu sudah sesuai! Jika belum sesuai, kamu bisa balik ke halaman sebelumnya.</Text>
        </View>

        {listPesananCard.map(x => x)}
    
    <View style={styles.subheaderView} >
        <Text style={styles.subTitle}>Ringkasan pesanan</Text>
    </View>

    <View style={styles.ringkasanContainer}>
        <View style={styles.ringkasan}>
            <Text style={styles.productPrice}>Jumlah item(s)</Text>
            <Text style={styles.productPrice}>{totalQuantity ? totalQuantity : 0}</Text>
        </View>
        <View style={styles.ringkasan}>
            <Text style={styles.productName}>Total harga</Text>
            <Text style={styles.productName}>Rp {totalPrice? totalPrice : 0}</Text>
        </View>
    </View>

    <TouchableOpacity style={styles.nextBtn} onPress={() => navigation.navigate('WaktuPemesanan',{orderList: listPesanan, itemList:listBarang, totalPrice: totalPrice, detailMerchant: route.params.detailMerchant})}>
        <Text style={styles.nextText}>Lanjut</Text>
    </TouchableOpacity>
    </ScrollView>
    
    
  );
}

export default ListPesananScreen

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    paddingTop: 80,
    width: "100%",
    height: "100%",
    backgroundColor: "#F9FAFF",
    paddingHorizontal: 20,
  },
 
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    alignSelf: 'flex-start', 
    color: "#3D4058",
  },

  headerView: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    paddingBottom: 15,
  },

productContainer: {
    flexDirection: 'row',
    marginVertical: 5,
},

productCard: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
},

productImage: {
    height: 50,
    width: 50,
    borderRadius: 6,
},

productDetail:{
    paddingHorizontal: 8, 
    justifyContent: 'space-evenly',
},

productName:{
    fontSize: 12, 
    fontWeight: 'bold', 
    color: "#3D4058",
},

productPrice:{
    fontSize: 12, 
    color: '#8D99AE',
},

subheaderView: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    paddingTop: 24,
    paddingBottom: 16,
},

subTitle:{
    fontWeight: 'bold',
    fontSize: 16,
    alignSelf: 'flex-start', 
    color: "#3D4058",
},

ringkasanContainer:{
    paddingBottom: 16,
},

ringkasan:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 8,
},

nextBtn: {
    width: "100%",
    borderRadius: 8,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    backgroundColor: "#EF233C",
  },

  nextText: {
    color: '#fff',
  },

  info:{
    backgroundColor:'#EDEFFF',
    padding: 4,
    marginBottom: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 8,
  },

  infoText:{
    color: '#545771',
    fontSize: 12,
    margin: 8,
    width: '90%'
  }
  
});

