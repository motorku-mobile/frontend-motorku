import React, {useState, useEffect} from 'react';
import {
  ActivityIndicator,
  View,
  StyleSheet,
  Image
} from 'react-native';
import { supabase } from '../components/Supabase';
 
const SplashScreen = ({navigation}) => {
  //State for ActivityIndicator animation
  const [animating, setAnimating] = useState(true);
        // CHECK LOGIN BLOCK
    async function LoginCheck(){
        const { data, error } = await supabase.auth.getSession();
        // Uncomment for debug
        // console.log(data);
        if(error){
            return null;
        }else{
            return data;
        }
    };
    
    useEffect(() => {
        setTimeout(async () => {
            setAnimating(false);
            //Check if user_id is set or not
            //If not then send for Authentication
            //else send to Home Screen
            let data = await LoginCheck();
            navigation.replace('Login');
        }, 3000);
  }, []);
 
  return (
    <View style={styles.container}>
      <Image
        source={require('../images/vector.png')}
        style={{width: '50%', resizeMode: 'contain', margin: 30}}
      />
      <ActivityIndicator
        animating={animating}
        color="#EF233C"
        size="large"
        style={styles.activityIndicator}
      />
    </View>
  );
};
 
export default SplashScreen;
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F9FAFF',
  },
  activityIndicator: {
    alignItems: 'center',
    height: 80,
  },
});