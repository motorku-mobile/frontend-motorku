import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
 
 
const ListPesananScreen = ({navigation}) => {
  return (
    <ScrollView style={styles.container}>
      <StatusBar style="auto" />

      <TouchableOpacity onPress={() => navigation.navigate('TambahMotor')}>
            <Image
                style={styles.tambah}
                source={require('../images/icon/add_circle_outline_icon.png')}
            />
    </TouchableOpacity>

      <View style={styles.headerView} >
        <Text style={styles.title}>Atur Garasimu</Text>
      </View>


      <TouchableOpacity onPress={() => navigation.navigate('DashboardMerchant')}>
      <View style={styles.motorCard}>
        <View style={styles.motorContainer}>
            <View>
                <Image style={styles.motorImage} source={require("../images/motor.png")} />
            </View>
            <View style={styles.motorDetail}>
                <Text style={styles.motorName}>Motor Bebek Kuning</Text>
                <Text style={styles.motorPrice}>B 1234 ABC</Text>
                <Text style={styles.motorPrice}>Yamaha</Text>
            </View>
        </View>
        <View style={{alignSelf: 'center', marginHorizontal: 4}}>
            <Image
                style={styles.icon}
                source={require('../images/icon/chevron_right_icon.png')}
            />
        </View>
    </View>
      </TouchableOpacity>

      <TouchableOpacity>
      <View style={styles.motorCard}>
        <View style={styles.motorContainer}>
            <View>
                <Image style={styles.motorImage} source={require("../images/motor.png")} />
            </View>
            <View style={styles.motorDetail}>
                <Text style={styles.motorName}>Motor Bebek Kuning</Text>
                <Text style={styles.motorPrice}>B 1234 ABC</Text>
                <Text style={styles.motorPrice}>Yamaha</Text>
            </View>
        </View>
        <View style={{alignSelf: 'center', marginHorizontal: 4}}>
            <Image
                style={styles.icon}
                source={require('../images/icon/chevron_right_icon.png')}
            />
        </View>
    </View>
      </TouchableOpacity>

    </ScrollView>
  );
}

export default ListPesananScreen

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    paddingTop: 80,
    width: "100%",
    height: "100%",
    backgroundColor: "#F9FAFF",
    paddingHorizontal: 20,
  },
 
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    alignSelf: 'center', 
    color: "#3D4058",
  },

  headerView: {
    width: "100%",
    flex: 1,
    paddingBottom: 15,
  },

motorContainer: {
    flexDirection: 'row',
    marginVertical: 5,
},

motorCard: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#ffffff',
    padding: 8,
    marginVertical: 4,
    borderWidth: 0.5,
    borderRadius: 8,
    borderColor: '#E8E8E8',
},

motorImage: {
    height: 60,
    width: 70,
    borderRadius: 8,
    marginHorizontal: 8,
},

motorDetail:{
    paddingHorizontal: 8, 
    height: 60,
    justifyContent: 'space-evenly'
},

motorName:{
    fontSize: 14, 
    fontWeight: 'bold', 
    color: "#3D4058",
},

motorPrice:{
    fontSize: 12, 
    color: '#8D99AE',
},

subheaderView: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    paddingTop: 24,
    paddingBottom: 16,
},

subTitle:{
    fontWeight: 'bold',
    fontSize: 16,
    alignSelf: 'flex-start', 
    color: "#3D4058",
},

ringkasanContainer:{
    paddingBottom: 16,
},

ringkasan:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 8,
},

nextBtn: {
    width: "100%",
    borderRadius: 8,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    backgroundColor: "#EF233C",
  },

  nextText: {
    color: '#fff',
  },

  info:{
    backgroundColor:'#EDEFFF',
    padding: 4,
    marginBottom: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderRadius: 8,
  },

  infoText:{
    color: '#545771',
    fontSize: 12,
    margin: 8,
    width: '90%'
  },

  icon:{
    width: 30,
    height: 30,
  },

  tambah:{
    alignSelf: 'flex-end', 
    position: 'absolute',
    width: 25,
    height: 25,
  }
  
});

