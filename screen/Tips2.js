import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
} from 'react-native';
 
const Tips2 = ({navigation}) => {
  return (
    <ScrollView style={styles.container}>
      <StatusBar style="auto" />

      <View style={styles.headerView} >
        <Text style={styles.title}>Merawat mesin</Text>
      </View>

      <Image
        style={styles.image}
        source={require('../images/tips1.jpg')}
      />
    
    <View style={styles.headerView} >
        <Text style={styles.subTitle}>Panaskan mobil setiap hari</Text>
        <Text style={styles.caption}>Usahakan untuk rutin memanaskan mobil setiap hari, setidaknya 30 detik hingga 1 menit tiap pagi. Mesin mobil yang rutin dipanaskan setiap hari, performanya akan lebih prima dan jarang muncul gangguan. Memanaskan mesin mobil setiap hari juga dapat membuat oli tetap merata ke seluruh mesin.</Text>
        <Text style={styles.caption}>Ingat, panaskan mesin mobil dengan transmisi di posisi netral dan tanpa menyalakan AC.</Text>
        <Text style={styles.subTitle}>Rutin memakai mobil</Text>
        <Text style={styles.caption}>Usahakan untuk rutin menggunakan mobil setiap hari. Jika tidak memakai mobil dalam waktu yang cukup lama, kamu usahakan untuk menggunakannya sesekali dalam sebulan. Hal ini juga sekaligus berguna untuk mendeteksi apakah terdapat perubahan dari kondisi mesin mobil kamu.</Text>
        <Text style={styles.caption}> </Text>
        <Text style={styles.caption}> </Text>
        <Text style={styles.caption}> </Text>
        <Text style={styles.caption}> </Text>
    </View>
    </ScrollView>
    
    
  );
}

export default Tips2

const styles = StyleSheet.create({
  container: {
    flexGrow: 3,
    paddingTop: 80,
    width: "100%",
    height: "100%",
    backgroundColor: "#F9FAFF",
    paddingHorizontal: 30,
  },
 
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    alignSelf: 'flex-start', 
    color: "#3D4058",
  },

  headerView: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    justifyContent: 'flex-end',
    paddingBottom: 15,
  },

  image: {
    resizeMode: 'cover',
    borderRadius: 16,
    width: "100%",
    height: '20%',
    marginBottom: 16,
  },

  loginBtn: {
    width: "50%",
    borderRadius: 16,
    height: 70,
    alignItems: "flex-start",
    paddingLeft: 16,
    justifyContent: "center",
    marginTop: 16,
    backgroundColor: "#3D4058",
  },

  loginText: {
    color: "#dadae8",
  },

  cardContainer: {
    flex: 1, 
    justifyContent: 'center', 
    display: 'flex', 
    flexDirection: 'row', 
    alignItems: "center", 
    marginTop: 16,
    marginBottom: 16,
  },

  card: {
    display: 'flex', 
    flexDirection: 'column', 
    alignItems: 'center', 
    width: '53%', 
    borderColor:'#dadae8', 
    borderWidth: 0.5, 
    padding: 10, 
    margin: 10, 
    borderRadius: 12,
    backgroundColor: '#3D4058',
  },

  cardAturGarasi: {
    display: 'flex', 
    flexDirection: 'column', 
    alignItems: 'center', 
    width: '40%', 
    borderColor:'#dadae8', 
    borderWidth: 0.5, 
    padding: 10, 
    margin: 10, 
    borderRadius: 12,
  },

  cardTitle: {
    fontSize: 14, 
    textAlignVertical: 'center', 
    fontWeight: 'bold', 
    color:"#dadae8", 
    marginBottom: 8
  },

  cardAturGarasiTitle: {
    fontSize: 14, 
    textAlignVertical: 'center', 
    fontWeight: 'bold', 
    color:"#3D4058", 
    marginBottom: 8
  },

  subTitle: {
    fontWeight: 'bold',
    fontSize: 16,
    alignSelf: 'flex-start', 
    color: "#3D4058",
    marginBottom: 8,
    marginTop: 8,
  },

  caption:{
    fontWeight: 'regular',
    fontSize: 14,
    alignSelf: 'flex-start', 
    color: "#8D99AE",
    marginBottom: 8,
  },

  tipsView:{
    flex:1,
  },

  imageTips:{
    flex: 1,
    padding: 32,
    borderRadius: 16,
    marginBottom: 16,
  }, 

  tips:{
    justifyContent: "flex-end",
  },

  tipsTitle:{
    fontWeight: 'bold',
    fontSize: 16,
    color: "#FFFFFF",
  },

  tipsCaption:{
    fontWeight: 'regular',
    fontSize: 12,
    color: "#FFFFFF",
  },

  imageTips2:{
    flex: 1,
    padding: 32,
    borderRadius: 16,
    marginBottom: 120,
  }, 
});

