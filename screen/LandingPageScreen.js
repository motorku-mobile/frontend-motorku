import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from '@expo/vector-icons/Ionicons';
import { supabase } from '../components/Supabase';

const LandingPageScreen = ({navigation}) => {
  const [profileName, setProfileName] = useState();

  async function LogOut(){
    let { error } = await supabase.auth.signOut()

    if(!error){
      navigation.navigate('Login')
    }else{
      alert(error);
    }
  }

      // CHECK LOGIN BLOCK
    async function LoginCheck(){
        const { data, error } = await supabase.auth.getSession();
        // Uncomment for debug
        // console.log(data);

        if(error){
            navigation.navigate('Login');
        }else{
            return data;
        }
    };

    useEffect(()=> {
        async function fetchData() {
          let data = await LoginCheck();
          let { data:pengguna, error2 } = await supabase
          .from('pengguna')
          .select()
          .like('email_pengguna', `%${data.session.user.email}%`);
            // Uncomment for debug
            // console.log(pengguna[0].nama);
            setProfileName(pengguna[0].nama);
        }
        fetchData();
    },[]);
    // END CHECK LOGIN BLOCK
  return (
    <ScrollView style={styles.container}>
      <StatusBar style="auto" />

      <View style={styles.headerTopView} >
        <Text style={styles.title}>Halo, {profileName}</Text>
        <TouchableOpacity onPress={() => LogOut()}>
          <Text style={styles.logout}>Logout</Text>
        </TouchableOpacity>
      </View>

      <TouchableOpacity>
      <Image
        style={styles.image}
        source={require('../images/motor.png')}
      ></Image>
      <View style={styles.motor}>
        <Text style={styles.namaMotor}>Motor Kuning tapi hitam</Text>
        <Text style={styles.detailMotor}>B 1234 ABC</Text>
        </View>
      </TouchableOpacity>
    
    <View style={styles.cardContainer}>
      <TouchableOpacity style={styles.card} onPress={() => navigation.navigate('Search')}> 
        
          {/* <FontAwesome name={'map'} color={'#dadae8'} size={40} style={{margin: 16}} /> */}
          <Image style={styles.iconSearch} source={require('../images/icon/search-icon.png')}/>
          <Text style={styles.cardTitle}>Cari Bengkel</Text>
      </TouchableOpacity>
    
      <TouchableOpacity style={styles.cardAturGarasi} onPress={() => navigation.navigate('AturGarasimu')}> 
          {/* <FontAwesome name={'motorcycle'} color={'#3D4058'} size={40} style={{margin: 16}} /> */}
          <Image style={styles.iconMotor} source={require('../images/icon/two_wheeler_icon.png')}/>
          <Text style={styles.cardAturGarasiTitle}>Atur Garasimu</Text>
      </TouchableOpacity>
    </View>

    <View style={styles.headerView} >
        <Text style={styles.subTitle}>Tips merawat kendaraanmu</Text>
        <Text style={styles.caption}>Berikut merupakan tips yang bisa kamu ikuti untuk merawat kendaraan kamu agar tetap awet</Text>
    </View>
    
    <View style={styles.tipsView} >
      <TouchableOpacity  onPress={() => navigation.navigate('Tips1')}>
      <ImageBackground
        style={styles.imageTips}
        resizeMode="cover"
        source={require('../images/tips3.jpg')}
      > 
      <View style={styles.tips} >
        <Text style={styles.tipsTitle}>Ganti ban dan cek tekanannya</Text>
        <Text style={styles.tipsCaption}>Fungsi ban bagi sebuah mobil memang sangat penting, dan bisa menentukan keselamatan berkendara..<Text style={{fontStyle : 'italic'}} > Read more...</Text></Text>
      </View>
      </ImageBackground>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => navigation.navigate('Tips2')}>
      <ImageBackground
        style={styles.imageTips}
        resizeMode="cover"
        source={require('../images/tips1.jpg')}
      > 
      <View style={styles.tips} >
        <Text style={styles.tipsTitle}>Merawat mesin</Text>
        <Text style={styles.tipsCaption}>Usahakan untuk rutin memanaskan mobil setiap hari, setidaknya 30 detik hingga 1 menit tiap pagi..<Text style={{fontStyle : 'italic'}} > Read more...</Text></Text>
      </View>
      </ImageBackground>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => navigation.navigate('Tips3')}>
      <ImageBackground
        style={styles.imageTips}
        resizeMode="cover"
        source={require('../images/tips2.jpg')}
      > 
      <View style={styles.tips} >
        <Text style={styles.tipsTitle}>Cek oli mesin</Text>
        <Text style={styles.tipsCaption}>Mengganti oli mesin memang rutin dilakukan saat mobilmu sudah menempuh jarak 5 ribu KM..<Text style={{fontStyle : 'italic'}} > Read more...</Text></Text>
      </View>
      </ImageBackground>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => navigation.navigate('Tips4')}>
      <ImageBackground
        style={styles.imageTips2}
        resizeMode="cover"
        source={require('../images/tips4.jpg')}
      > 
      <View style={styles.tips} >
        <Text style={styles.tipsTitle}>Merawat aki</Text>
        <Text style={styles.tipsCaption}>Aki mobil ada dua yaitu tipe aki basah dan tipe maintenance free alias aki kering. Fungsi aki mobil sangat vital..<Text style={{fontStyle : 'italic'}} > Read more...</Text></Text>
      </View>
      </ImageBackground>
      </TouchableOpacity>

    </View>
    </ScrollView>
    
    
  );
}

export default LandingPageScreen

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    paddingTop: 80,
    width: "100%",
    height: "100%",
    backgroundColor: "#F9FAFF",
    paddingHorizontal: 20,
  },
 
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    alignSelf: 'flex-start', 
    color: "#3D4058",
  },

  motor:{
    position: 'absolute', 
    top: 0, 
    left: 0, 
    right: 0, 
    bottom: 0, 
    justifyContent: 'flex-end', 
    alignItems: 'flex-start',
    margin: 24,
  },

  namaMotor:{
    fontSize: 20,
    color: '#FFFFFF',
    fontWeight: 'bold',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10
  },

  
  detailMotor:{
    fontSize: 16,
    color: '#ECECEE',
    fontWeight: 'regular',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10
  },

  headerView: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    justifyContent: 'flex-end',
    paddingBottom: 15,
  },

  image: {
    resizeMode: 'cover',
    borderRadius: 16,
    width: "100%",
  },

  loginBtn: {
    width: "50%",
    borderRadius: 16,
    height: 70,
    alignItems: "flex-start",
    paddingLeft: 16,
    justifyContent: "center",
    marginTop: 16,
    backgroundColor: "#3D4058",
  },

  loginText: {
    color: "#dadae8",
  },

  cardContainer: {
    flex: 1, 
    justifyContent: 'center', 
    display: 'flex', 
    flexDirection: 'row', 
    alignItems: "center", 
    marginTop: 24,
    marginBottom: 24,
  },

  card: {
    display: 'flex', 
    flexDirection: 'column', 
    alignItems: 'center', 
    width: '53%', 
    borderColor:'#dadae8', 
    borderWidth: 0.5, 
    padding: 10, 
    margin: 10, 
    borderRadius: 12,
    backgroundColor: '#3D4058',
  },

  cardAturGarasi: {
    display: 'flex', 
    flexDirection: 'column', 
    alignItems: 'center', 
    width: '40%', 
    borderColor:'#dadae8', 
    borderWidth: 0.5, 
    padding: 10, 
    margin: 10, 
    borderRadius: 16,
  },

  cardTitle: {
    fontSize: 12, 
    textAlignVertical: 'center', 
    fontWeight: 'bold', 
    color:"#dadae8", 
    marginBottom: 8
  },

  cardAturGarasiTitle: {
    fontSize: 12, 
    textAlignVertical: 'center', 
    fontWeight: 'bold', 
    color:"#3D4058", 
    marginBottom: 8
  },

  subTitle: {
    fontWeight: 'bold',
    fontSize: 16,
    alignSelf: 'flex-start', 
    color: "#3D4058",
    marginBottom: 8,
  },

  caption:{
    fontWeight: 'regular',
    fontSize: 12,
    alignSelf: 'flex-start', 
    color: "#8D99AE",
  },

  tipsView:{
    flex:1,
  },

  imageTips:{
    flex: 1,
    padding: 32,
    borderRadius: 16,
    marginBottom: 16,
  }, 

  tips:{
    justifyContent: "flex-end",
  },

  tipsTitle:{
    fontWeight: 'bold',
    fontSize: 14,
    color: "#FFFFFF",
  },

  tipsCaption:{
    fontWeight: 'regular',
    fontSize: 12,
    color: "#FFFFFF",
  },

  imageTips2:{
    flex: 1,
    padding: 32,
    borderRadius: 16,
    marginBottom: 120,
  }, 

  headerTopView:{
    width: "100%",
    flex: 1,
    alignItems: "center",
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingBottom: 15,
  },

  logout:{
    fontWeight: 'regular',
    fontSize: 12,
    color: "#EF233C",
  },

  iconSearch:{
    width: '20%',
    height: '50%',
    margin: 16,
  },

  iconMotor:{
    width: '30%',
    height: '50%',
    margin: 16,
  }
});

