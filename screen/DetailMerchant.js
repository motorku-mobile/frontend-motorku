import React, {useState, useEffect} from 'react';
import { StatusBar } from 'expo-status-bar';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollViewBase,
} from 'react-native';
import { supabase } from '../components/Supabase';
import { Divider, ListItem } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';

const MerchantDetail = ({ route, navigation }) => {

    const [dataMerchant, setDataMerchant] = useState();
    const [listProduk, setListProduk] = useState([]);
    var orderList = {};

    const Item = ({ item }) => {
        const [amount, setAmount] = useState(item.num);
    
        return(
            <View style={styles.item }>
                <View style={{ flexDirection: 'row' }}>
                    <Image style={styles.itemThumb} source={require("../assets/icon.png")} />
                    <View style={{ paddingHorizontal: 10}}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: "#3D4058" }}>{item.name}</Text>
                        <Text style={{ fontSize: 14, color: '#8D99AE' }}>Rp {item.price}</Text>
                        <Text style={{ fontSize: 12, color: '#8D99AE' }}>{item.notes}</Text>
                    </View>
                </View>
                <View style={styles.itemCounter} >
                    <TouchableOpacity style={ amount < 1 ? styles.invalidBtn : styles.validBtn } disabled={ amount < 1 ? true : false} onPress={() => {orderList[item?.id] ? orderList[item.id] -= 1 : orderList[item.id] = 0; setAmount(amount - 1)}}>
                        <Text style={ amount < 1 ? { color : '#bbb' } : { color : '#3D4058' } }>{'<'}</Text>
                    </TouchableOpacity>
                    <Text style={{ marginHorizontal: 10 }}>{amount}</Text>
                    <TouchableOpacity style={styles.validBtn} onPress={() => {orderList[item?.id] ? orderList[item.id] += 1 : orderList[item.id] = 1; setAmount(amount+1)}}>
                        <Text>{'>'}</Text>
                    </TouchableOpacity>
                </View>
            </View>
          );
    }

    // CHECK LOGIN BLOCK
    async function LoginCheck(){
        const { data, error } = await supabase.auth.getSession();
        // console.log(data);
        if(error){
            navigation.navigate('Login');
        }else{
            return data;
        }
    }

    async function handleItem(){
        let {data: barang, error} = await supabase
        .from('barang')
        .select()
        .eq('merchant', route.params.merchant_id);
        // console.log(route.params.merchant_id);
        // console.log(barang);

        barang.forEach(x => {
            setListProduk([...listProduk, 
                {
                    id: x.id,
                    name: x.nama_barang,
                    price: x.harga,
                    notes: x.deskripsi,
                    num: 0,
                    pic: "../assets/icon.png"
                }
            ])
        });

        let {data: jasa, error2} = await supabase
        .from('jasa')
        .select()
        .eq('merchant', route.params.merchant_id);

        jasa.forEach(x => {
            setListProduk([...listProduk, 
                {
                    id: x.id,
                    name: x.nama_jasa,
                    price: x.harga,
                    notes: x.deskripsi,
                    num: 0,
                    pic: "../assets/icon.png"
                }
            ])
        });
    }

    async function getDataMerchant() {
        let merchantId = route.params.merchant_id;

        // console.log(merchantId);

        let { data: merchant, error } = await supabase
        .from('merchant')
        .select()
        .eq('id', merchantId);

        setDataMerchant(merchant[0]);
        // console.log(dataMerchant);
    }

    useEffect(()=> {
        async function fetchData() {
            let data = await LoginCheck();
            // console.log(data);
            await getDataMerchant();
        }
        fetchData();
        setListProduk([]);
    },[])
    // END CHECK LOGIN BLOCK

    useEffect(() => {
        handleItem();
    }, [route.params.merchant_id])

    function handleOrderChanges() {
        var itemListJson = ListProduk
    }
    
    const renderItem = ({ item }) => {
        return (
          <Item item={item}/>
        );
    };

    return(

        <View style={styles.container}>
            <StatusBar style="auto" />

            <Image style={styles.coverPic} source={require("../assets/default-merc.png")} />
            <View style={styles.tray}>
                <View style={styles.mercHeader}>
                    <Text style={{ flex: 3, fontWeight: 'bold', fontSize: 20, color: "#3D4058" }}>{dataMerchant?.nama_merchant ? dataMerchant.nama_merchant : ""}</Text>
                    <Text style={{ flex: 1, fontSize: 14, color: '#8D99AE'}}>{dataMerchant?.jam_buka ? dataMerchant.jam_buka : "00.00"} - {dataMerchant?.jam_tutup ? dataMerchant.jam_tutup : "00.00"}</Text>
                </View>
                <View style={styles.mercHeader}>
                    <Text style={{ flex: 3, color: '#8D99AE'}}>{dataMerchant?.alamat ? dataMerchant.alamat : ""}</Text>
                    <View style={{ flex: 1}}/>
                </View>
                <Divider style={{ marginVertical: 10 }} />
                <Text style={{ fontWeight: 'medium', fontSize: 16, marginBottom: 5, color: "#3D4058" }}>List Produk</Text>
                <FlatList
                    data={listProduk}
                    renderItem={renderItem}
                    keyExtractor={(item) => item.id}
                />
                <TouchableOpacity style={styles.CheckoutBtn} onPress={() => navigation.navigate('ListPesanan',{orderList: orderList, itemList:listProduk, detailMerchant: dataMerchant})}>
                    <Text style={{ color: '#fff' }}>Order Now</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}

export default MerchantDetail;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      width: "100%",
      height: "100%",
      backgroundColor: "#fff",
      alignItems: 'center'
    },

    coverPic: {
        width: '100%',
        height: 225,
    },

    tray: {
        backgroundColor: '#F9FAFF',
        width: '100%',
        flex: 1,
        marginTop: -20,
        borderTopEndRadius: 25,
        borderTopStartRadius: 25,
        paddingHorizontal: 20,
        paddingVertical: 25,
    },

    mercHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 5,
    },

    item: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#dadae8',
        paddingVertical: 12,
        paddingHorizontal: 10,
        marginVertical: 5,
    },

    itemThumb: {
        height: 50,
        width: 80,
        borderRadius: 6,
    },

    itemCounter: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        height: '100%',
        width: 50,
        marginEnd: 5,
    }, 

    invalidBtn: {
        color: '#bbb',
        padding: 5,
        borderStyle: 'solid',
        borderColor: '#bbb',
        borderWidth: 1,
        borderRadius: 50
    },

    validBtn: {
        padding: 5,
        borderStyle: 'solid',
        borderColor: '#000',
        borderWidth: 1,
        borderRadius: 50
    },
    
    CheckoutBtn: {
        width: "100%",
        borderRadius: 12,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 20,
        backgroundColor: "#EF233C",
      },
});