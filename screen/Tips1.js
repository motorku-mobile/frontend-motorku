import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
} from 'react-native';
 
const Tips1 = ({navigation}) => {
  return (
    <ScrollView style={styles.container}>
      <StatusBar style="auto" />

      <View style={styles.headerView} >
        <Text style={styles.title}>Ganti ban dan cek tekanannya</Text>
      </View>

      <Image
        style={styles.image}
        source={require('../images/tips3.jpg')}
      />
    
    <View style={styles.headerView} >
        <Text style={styles.subTitle}>Ganti ban</Text>
        <Text style={styles.caption}>Fungsi ban bagi sebuah mobil memang sangat penting, dan bisa menentukan keselamatan berkendara. Maka sudah wajib hukumnya pemilik mobil untuk mengerti cara mengganti ban mobil.</Text>
        <Text style={styles.caption}>Kalau ngelepas ban saja gak bisa, ya gimana kalau tiba-tiba ban bocor di tengah jalan? Masa iya mau minta tolong orang pasang ban serep.</Text>
        <Text style={styles.caption}>Dalam proses pencopotan ban, kamu juga harus paham cara menggunakan dongkrak. Buat yang memang belum bisa, gak apa-apa, masih ada waktu untuk mempelajari cara dongkrak mobil.</Text>
        <Text style={styles.subTitle}>Cek tekanan angin</Text>
        <Text style={styles.caption}>Selain bongkar pasang ban, kamu juga harus memahami tekanan angin ban mobil yang ideal. Pastikan untuk mengetahui tekanan standar ban mobilmu di buku manual.</Text>
        <Text style={styles.caption}>Setelah itu, coba tendang ban mobilmu dengan kaki. Bila bunyi yang dihasilkan keras dan pantulannya kuat, maka bisa dikatakan tekanan banmu terlalu tinggi. Namun jika malah sebaliknya, ya jelas banmu kurang angin.</Text>
        <Text style={styles.caption}>So, jangan lupa dicek ya tekanannya. Apalagi kalau habis pulang mudik, touring, atau perjalanan jauh beratus-ratus kilometer.</Text>
        <Text style={styles.caption}> </Text>
        <Text style={styles.caption}> </Text>
        <Text style={styles.caption}> </Text>
        <Text style={styles.caption}> </Text>
        <Text style={styles.caption}> </Text>
    </View>

    

    </ScrollView>
    
    
  );
}

export default Tips1

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    paddingTop: 80,
    width: "100%",
    height: "100%",
    backgroundColor: "#F9FAFF",
    paddingHorizontal: 30,
  },
 
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    alignSelf: 'flex-start', 
    color: "#3D4058",
  },

  headerView: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    justifyContent: 'flex-end',
    paddingBottom: 15,
  },

  image: {
    resizeMode: 'cover',
    borderRadius: 16,
    width: "100%",
    height: '20%',
    marginBottom: 16,
  },

  loginBtn: {
    width: "50%",
    borderRadius: 16,
    height: 70,
    alignItems: "flex-start",
    paddingLeft: 16,
    justifyContent: "center",
    marginTop: 16,
    backgroundColor: "#3D4058",
  },

  loginText: {
    color: "#dadae8",
  },

  cardContainer: {
    flex: 1, 
    justifyContent: 'center', 
    display: 'flex', 
    flexDirection: 'row', 
    alignItems: "center", 
    marginTop: 16,
    marginBottom: 16,
  },

  card: {
    display: 'flex', 
    flexDirection: 'column', 
    alignItems: 'center', 
    width: '53%', 
    borderColor:'#dadae8', 
    borderWidth: 0.5, 
    padding: 10, 
    margin: 10, 
    borderRadius: 12,
    backgroundColor: '#3D4058',
  },

  cardAturGarasi: {
    display: 'flex', 
    flexDirection: 'column', 
    alignItems: 'center', 
    width: '40%', 
    borderColor:'#dadae8', 
    borderWidth: 0.5, 
    padding: 10, 
    margin: 10, 
    borderRadius: 12,
  },

  cardTitle: {
    fontSize: 14, 
    textAlignVertical: 'center', 
    fontWeight: 'bold', 
    color:"#dadae8", 
    marginBottom: 8
  },

  cardAturGarasiTitle: {
    fontSize: 14, 
    textAlignVertical: 'center', 
    fontWeight: 'bold', 
    color:"#3D4058", 
    marginBottom: 8
  },

  subTitle: {
    fontWeight: 'bold',
    fontSize: 16,
    alignSelf: 'flex-start', 
    color: "#3D4058",
    marginBottom: 8,
    marginTop: 8,
  },

  caption:{
    fontWeight: 'regular',
    fontSize: 14,
    alignSelf: 'flex-start', 
    color: "#8D99AE",
    marginBottom: 8,
  },

  tipsView:{
    flex:1,
  },

  imageTips:{
    flex: 1,
    padding: 32,
    borderRadius: 16,
    marginBottom: 16,
  }, 

  tips:{
    justifyContent: "flex-end",
  },

  tipsTitle:{
    fontWeight: 'bold',
    fontSize: 16,
    color: "#FFFFFF",
  },

  tipsCaption:{
    fontWeight: 'regular',
    fontSize: 12,
    color: "#FFFFFF",
  },

  imageTips2:{
    flex: 1,
    padding: 32,
    borderRadius: 16,
    marginBottom: 120,
  }, 
});

