import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
} from 'react-native';
 
const Tips3 = ({navigation}) => {
  return (
    <ScrollView style={styles.container}>
      <StatusBar style="auto" />

      <View style={styles.headerView} >
        <Text style={styles.title}>Cek oli mesin</Text>
      </View>

      <Image
        style={styles.image}
        source={require('../images/tips2.jpg')}
      />
    
    <View style={styles.headerView} >
        {/* <Text style={styles.subTitle}>Panaskan mobil setiap hari</Text> */}
        <Text style={styles.caption}>Mengganti oli mesin memang rutin dilakukan saat mobilmu sudah menempuh jarak 5 ribu KM. Tapi kadang, ada kalanya waktu ganti oli molor mengingat adanya kesibukan.</Text>
        <Text style={styles.caption}>Sebagai pemilik mobil, wajib dong hukumnya untuk bisa mengecek oli mesin sendiri. Sebenarnya sih cara mengecek oli itu mudah banget, kamu cuma butuh menghidupkan mesin mobil saja. Jika indikator oli di dashboard sudah mati, maka olimu masih dalam keadaan yang baik.</Text>
        <Text style={styles.caption}>Tapi agar lebih akurat, maka kamu harus melakukan hal berikut. Begini langkahnya.</Text>

        <Text style={styles.caption}>{'\u2022'} Matikan mesin mobil dan tunggu selama kurang lebih 10 menit</Text>
        <Text style={styles.caption}>{'\u2022'} Buka kap mesin dan tarik stik oli (di atas bongkahan mesin)</Text>
        <Text style={styles.caption}>{'\u2022'} Bersihkan ujung stik dengan lap </Text>
        <Text style={styles.caption}>{'\u2022'} Masukan lagi stiknya ke dalam dan tarik lagi </Text>
        <Text style={styles.caption}>{'\u2022'} Lihat stik olinya, kamu bisa membaca kadar olimu lewat stik ini, rendah, normal, atau kebanyakan. </Text>
        <Text style={styles.caption}>{'\u2022'} Bila ada di bagian L atau 1, maka kondisi olimu berkurang. Di posisi dua artinya normal, dan jika di atas angka 3 atau F maka tandanya kepenuhan. </Text>
        <Text style={styles.caption}> </Text>
        <Text style={styles.caption}> </Text>
        <Text style={styles.caption}> </Text>
        <Text style={styles.caption}> </Text>
        <Text style={styles.caption}> </Text>
        
    </View>

    

    </ScrollView>
    
    
  );
}

export default Tips3

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    paddingTop: 80,
    width: "100%",
    height: "100%",
    backgroundColor: "#F9FAFF",
    paddingHorizontal: 30,
  },
 
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    alignSelf: 'flex-start', 
    color: "#3D4058",
  },

  headerView: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    justifyContent: 'flex-end',
    paddingBottom: 15,
  },

  image: {
    resizeMode: 'cover',
    borderRadius: 16,
    width: "100%",
    height: '20%',
    marginBottom: 16,
  },

  loginBtn: {
    width: "50%",
    borderRadius: 16,
    height: 70,
    alignItems: "flex-start",
    paddingLeft: 16,
    justifyContent: "center",
    marginTop: 16,
    backgroundColor: "#3D4058",
  },

  loginText: {
    color: "#dadae8",
  },

  cardContainer: {
    flex: 1, 
    justifyContent: 'center', 
    display: 'flex', 
    flexDirection: 'row', 
    alignItems: "center", 
    marginTop: 16,
    marginBottom: 16,
  },

  card: {
    display: 'flex', 
    flexDirection: 'column', 
    alignItems: 'center', 
    width: '53%', 
    borderColor:'#dadae8', 
    borderWidth: 0.5, 
    padding: 10, 
    margin: 10, 
    borderRadius: 12,
    backgroundColor: '#3D4058',
  },

  cardAturGarasi: {
    display: 'flex', 
    flexDirection: 'column', 
    alignItems: 'center', 
    width: '40%', 
    borderColor:'#dadae8', 
    borderWidth: 0.5, 
    padding: 10, 
    margin: 10, 
    borderRadius: 12,
  },

  cardTitle: {
    fontSize: 14, 
    textAlignVertical: 'center', 
    fontWeight: 'bold', 
    color:"#dadae8", 
    marginBottom: 8
  },

  cardAturGarasiTitle: {
    fontSize: 14, 
    textAlignVertical: 'center', 
    fontWeight: 'bold', 
    color:"#3D4058", 
    marginBottom: 8
  },

  subTitle: {
    fontWeight: 'bold',
    fontSize: 16,
    alignSelf: 'flex-start', 
    color: "#3D4058",
    marginBottom: 8,
    marginTop: 8,
  },

  caption:{
    fontWeight: 'regular',
    fontSize: 14,
    alignSelf: 'flex-start', 
    color: "#8D99AE",
    marginBottom: 8,
  },

  tipsView:{
    flex:1,
  },

  imageTips:{
    flex: 1,
    padding: 32,
    borderRadius: 16,
    marginBottom: 16,
  }, 

  tips:{
    justifyContent: "flex-end",
  },

  tipsTitle:{
    fontWeight: 'bold',
    fontSize: 16,
    color: "#FFFFFF",
  },

  tipsCaption:{
    fontWeight: 'regular',
    fontSize: 12,
    color: "#FFFFFF",
  },

  imageTips2:{
    flex: 1,
    padding: 32,
    borderRadius: 16,
    marginBottom: 120,
  }, 
});

