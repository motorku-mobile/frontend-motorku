import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { supabase } from '../components/Supabase';
 
const RegisterPenggunaScreen = ({navigation}) => {
  const [namaLengkap, setNamaLengkap] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [alamat, setAlamat] = useState("");
  const [telp, setTelp] = useState("");

  const handleSubmitPress = async () => {
    if (!namaLengkap) {
      alert('Please fill your Complete Name');
      return;
    }

    if (!email) {
      alert('Plelase fill your Email');
      return;
    }

    if (!password) {
      alert('Please fill your Password');
      return;
    }

    if (!confirmPassword) {
      alert('Please fill Rewrite your Confirmation Password');
      return;
    }

    if(!alamat) {
      alert('Please fill your Address');
      return;
    }

    if (!telp) {
      alert('Please fill your Phone Number');
      return;
    }

    if(password == confirmPassword){

      var { error } = await supabase.auth.signUp({
        email: email,
        password: password,
        phone: telp,
      })

      if(error){
        // Uncomment for debug
        // console.log("Error Log Proses Registrasi Merchant");
        // console.log(error);
        alert('There is a error in your registration process');
        return;
      }else{
        var {error} = await supabase
        .from('pengguna')
        .insert([{
          nama: namaLengkap,
          alamat: alamat, 
          email_pengguna: email,
        }]);
  
        if (error) {
          alert('There is a error in your registration process');
          return;
        }else{
          alert('Your Account Has Been Registered');
          navigation.navigate('Login');
        }
      }
    }
  }

  return (
    <ScrollView style={styles.container}>
      <StatusBar style="auto" />

      <View style={styles.headerView} >
        <Text style={styles.title}>Register sebagai pengguna</Text>
      </View>

      <View style={styles.formView}>
        <View style={styles.inputView}>
          <Text style={styles.inputLabel}>Nama Lengkap</Text>
          <TextInput
            style={styles.TextInput}
            placeholder="ex. John Doe"
            placeholderTextColor="#BABABB"
            keyboardType="email-address"
            returnKeyType="next"
            onChangeText={(namaLengkap) => setNamaLengkap(namaLengkap)}
          />
        </View>

        <View style={styles.inputView}>
          <Text style={styles.inputLabel}>Email</Text>
          <TextInput
            style={styles.TextInput}
            placeholder="ex. john.doe@example.com"
            placeholderTextColor="#BABABB"
            onChangeText={(email) => setEmail(email)}
          />
        </View>

        <View style={styles.inputView}>
          <Text style={styles.inputLabel}>Password</Text>
          <TextInput
            style={styles.TextInput}
            placeholder="At least 6 characters"
            placeholderTextColor="#BABABB"
            secureTextEntry={true}
            onChangeText = {(password) => setPassword(password)}
          />
        </View>

        <View style={styles.inputView}>
          <Text style={styles.inputLabel}>Ulangi Password</Text>
          <TextInput
            style={styles.TextInput}
            placeholder="At least 6 characters"
            placeholderTextColor="#BABABB"
            secureTextEntry={true}
            onChangeText= {(confirmPassword) => setConfirmPassword(confirmPassword)}
          />
        </View>

        <View style={styles.inputView}>
          <Text style={styles.inputLabel}>Alamat</Text>
          <TextInput
            style={styles.TextInput}
            multiline
            placeholder="ex. Jalan Mawar"
            placeholderTextColor="#BABABB"
            onChangeText={(alamat) => setAlamat(alamat)}
          />
        </View>

        <View style={styles.inputView}>
          <Text style={styles.inputLabel}>Nomor Telepon</Text>
          <TextInput
            style={styles.TextInput}
            multiline
            placeholder="ex. 0812xxxxxxxx"
            placeholderTextColor="#BABABB"
            onChangeText={(telp) => setTelp(telp)}
          />
        </View>

        <TouchableOpacity 
          style={styles.registerBtn}
          onPress={handleSubmitPress}>
          <Text style={styles.registerText}>Register</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

export default RegisterPenggunaScreen

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    paddingTop: 80,
    width: "100%",
    height: "100%",
    backgroundColor: "#F9FAFF",
    paddingHorizontal: 30,
  },
 
  title: {
    fontWeight: 'bold',
    fontSize: 24,
    alignSelf: 'flex-start', 
    color: "#3D4058",
  },

  headerView: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    justifyContent: 'flex-end',
    paddingBottom: 15,
  },

  formView: {
    width: "100%",
    flex: 6,
  },

  inputView: {
    flexDirection: 'column',
    height: 80,
    marginTop: 20,
  },

  inputLabel: {
    fontWeight: 'bold',
    fontSize: 14,
    flex: 2,
    color: '#3D4058',
  },

  TextInput: {
    alignItems: 'stretch',
    flex: 3,
    color: 'black',
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 12,
    borderColor: '#dadae8',
  },
 
  registerBtn: {
    width: "100%",
    borderRadius: 12,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 150,
    backgroundColor: "#EF233C",
  },

  registerText: {
    color: '#fff',
  },

  RedText:{
    color:"#EF233C",
  }
});

