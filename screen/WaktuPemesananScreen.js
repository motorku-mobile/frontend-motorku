import { StatusBar } from 'expo-status-bar';
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Button,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import DatePicker from 'react-native-modern-datepicker';
import { supabase } from '../components/Supabase';
 
const WaktuPemesananScreen = ({route, navigation}) => {

    const [selectedDate, setSelectedDate] = useState('');
    const [profileName, setProfileName] = useState();

    // CHECK LOGIN BLOCK
    async function LoginCheck(){
        const { data, error } = await supabase.auth.getSession();
        // Uncomment for debug
        // console.log(data);

        if(error){
            navigation.navigate('Login');
        }else{
            return data;
        }
    };

    useEffect(()=> {
        async function fetchData() {
          let data = await LoginCheck();
          let { data:pengguna, error2 } = await supabase
          .from('pengguna')
          .select()
          .like('email_pengguna', `%${data.session.user.email}%`);
            // Uncomment for debug
            // console.log(pengguna[0].nama);
            setProfileName(pengguna[0].nama);
        }
        fetchData();
    },[]);
    // END CHECK LOGIN BLOCK

    const currentDatetime = new Date();
    const currentDate = currentDatetime.getDate();
    const currentMonth = currentDatetime.getMonth() + 1;
    const currentYear = currentDatetime.getFullYear();

    const handleSubmitPress = async () => {
        if (!selectedDate) {
            alert('Please choose date and time!');
        } else {
            // TODO

            const { data, error } = await supabase
              .from('order')
              .insert([
                { order_creator: profileName, merchant_destination: route.params.detailMerchant.email_merchant , order_time: selectedDate, total: route.params.totalPrice, list_order:route.params.orderList},
              ])

            if(!error){
              alert('Your Order Has Been Created!');
              navigation.navigate('LandingUser');
            }

        }
    }

    const currentDatetimeString = currentYear + '-' + currentMonth + '-' + currentDate ;

  return (
    <ScrollView style={styles.container}>
      <StatusBar style="auto" />

      <View style={styles.headerView} >
        <Text style={styles.title}>Waktu dan Tanggal Pemesanan</Text>
      </View>
        
        <View style={styles.datepickerContainer}>
        <DatePicker
            onSelectedChange={date => setSelectedDate(date)}
            options={{
                backgroundColor: '#FFFFFF',
                textHeaderColor: '#3D4058',
                textDefaultColor: '#8D99AE',
                selectedTextColor: '#fff',
                mainColor: '#EF233C',
                textSecondaryColor: '#BABABB',
                borderColor: 'rgba(122, 146, 165, 0.1)',
            }}
            current={currentDatetimeString}
            minimumDate={currentDatetimeString}
            selected={currentDatetimeString}
            style={{borderRadius: 16}}
    />
    <View style={styles.dateContainer}>
        <Text style={styles.date}>Tanggal yang dipilih : <Text style={{fontWeight:'bold'}}>{selectedDate}</Text></Text>
    </View>
    
        </View>
    <TouchableOpacity style={styles.nextBtn}  onPress={handleSubmitPress}>
        <Text style={styles.nextText}>Pesan</Text>
    </TouchableOpacity>

    </ScrollView>
  );
}

export default WaktuPemesananScreen

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    paddingTop: 80,
    width: "100%",
    height: "100%",
    backgroundColor: "#F9FAFF",
    paddingHorizontal: 20,
  },
 
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    alignSelf: 'flex-start', 
    color: "#3D4058",
  },

  headerView: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    paddingBottom: 15,
  },

  formView: {
    width: "100%",
    flex: 6,
  },

  inputView: {
    flexDirection: 'column',
    height: 80,
    marginTop: 20,
  },

  inputLabel: {
    fontWeight: 'bold',
    fontSize: 14,
    flex: 2,
    color: '#3D4058',
  },

  TextInput: {
    alignItems: 'stretch',
    flex: 3,
    color: 'black',
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 12,
    borderColor: '#dadae8',
  },

  nextBtn: {
    width: "100%",
    borderRadius: 8,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    backgroundColor: "#EF233C",
  },

  nextText: {
    color: '#fff',
  },
  
  datepickerContainer:{
    paddingBottom: 16,
  },

  dateContainer:{
    paddingTop: 16,
  },

  date:{
    color: '#8D99AE'
  }
});

