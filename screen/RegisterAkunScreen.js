import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
 
 
const RegisterAkunScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <View style={styles.headerView} >
        <Image style={styles.image} source={require("../images/register.png")} />
        <Text style={styles.LeftTitle}>Register</Text>
        <Text style={styles.LeftDesc}>Anda ingin mendaftarkan akun sebagai..</Text>
      </View>
      <View style={{flex: 1, height: 60, justifyContent: 'center', display: 'flex', flexDirection: 'row', alignItems: "center", padding: 10}}>
          <View style={{display: 'flex', flexDirection: 'column', alignItems: 'center', width: '50%', borderColor:'#dadae8', borderWidth: 0.5, padding: 10, margin: 10, borderRadius: 12}}>
          <Image style={{width: 60, height: 60, margin: 16}} source={require("../images/icon/person_outline_icon.png")}/>
            <Text style={{fontSize: 18, textAlignVertical: 'center', fontWeight: 'bold', color:"#3D4058", marginBottom: 8}}>Pengguna</Text>
            <Text style={{fontSize: 12, textAlignVertical: 'center', fontWeight: 'regular', color:"#8D99AE", marginBottom: 8}}>Pengguna bisa melihat riwayat perawatan, mengorder pesanan, memantau pesanan, dan membatalkan pesanan dari pengguna.</Text>
            <TouchableOpacity style={styles.loginBtn} onPress={() => navigation.navigate('RegisUser')}>
              <Text style={styles.loginText}>Register</Text>
            </TouchableOpacity>
          </View>
          <View style={{display: 'flex', flexDirection: 'column', alignItems: 'center',  width: '50%', borderColor:'#dadae8', borderWidth: 0.5, padding: 10, borderRadius: 12}}>
            <Image style={{width: 60, height: 60, margin: 16}}  source={require("../images/icon/storefront_icon.png")}/>
            <Text style={{fontSize: 18, textAlignVertical: 'center', fontWeight: 'bold', color:"#3D4058", marginBottom: 8}}>Merchant</Text>
            <Text style={{fontSize: 12, textAlignVertical: 'center', fontWeight: 'regular', color:"#8D99AE", marginBottom: 8}}>Pemilik merchant bisa mengatur produk atau jasa yang ditawarkan, mengatur pesanan yang masuk, dan lain sebagainya.</Text>
            <TouchableOpacity style={styles.loginBtn} onPress={() => navigation.navigate('RegisMerc')}>
              <Text style={styles.loginText}>Register</Text>
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity style={styles.forgot_button} onPress={() => navigation.navigate('Login')}>
          <Text >Sudah Mempunyai Akun?  <Text style={{color: "#EF233C"}}>Masuk sekarang</Text></Text>
        </TouchableOpacity>
    </View>
  );
}

export default RegisterAkunScreen
 
const styles = StyleSheet.create({
  container: {
    flex: 2,
    width: "100%",
    height: "100%",
    backgroundColor: "#F9FAFF",
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 30,
  },

  image: {
    width: 250,
    height: 250,
    resizeMode: 'contain',
  },

  LeftTitle: {
    fontWeight: 'bold',
    fontSize: 24,
    alignSelf: 'flex-start', 
    color:"#3D4058"
  },

  LeftDesc: {
    fontWeight: 'regular',
    fontSize: 14,
    alignSelf: 'flex-start', 
    marginTop: 8,
    color:"#3D4058"
  },


  headerView: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    justifyContent: 'flex-end',
  },

  ScreenTitle: {
    fontWeight: 'bold',
    fontSize: 24,
  },

  loginBtn: {
    width: "100%",
    borderRadius: 8,
    height: 32,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    backgroundColor: "#EF233C",
  },

  loginText: {
    color: '#fff',
  },

  card: {
    width: "50%",
  },
  
  forgot_button: {
    height: 100,
    marginTop: 10,
    alignItems:'center',
    color: '#8D99AE',
  },
});