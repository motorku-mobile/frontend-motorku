import 'react-native-url-polyfill/auto'
import {createClient} from '@supabase/supabase-js';
import AsyncStorage from '@react-native-async-storage/async-storage';

const supabaseUrl = 'https://qxhdmqtmvhbripswdnep.supabase.co';
const supabaseAnonKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InF4aGRtcXRtdmhicmlwc3dkbmVwIiwicm9sZSI6InNlcnZpY2Vfcm9sZSIsImlhdCI6MTY2NTA0MTEyOSwiZXhwIjoxOTgwNjE3MTI5fQ.6TbHLawoDMMc44iQQ29pdqjfiXsT2RepbuC7Uh8dLes';

export const supabase = createClient(supabaseUrl, supabaseAnonKey,{
    auth: {
      storage: AsyncStorage,
      autoRefreshToken: true,
      persistSession: true,
      detectSessionInUrl: false,
    },
  });