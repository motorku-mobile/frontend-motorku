import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import RegisterPenggunaScreen from './screen/RegisterPenggunaScreen';
import LandingPageScreen from './screen/LandingPageScreen';
import RegisterMerchantScreen from './screen/RegisterMerchantScreen';
import RegisterAkunScreen from './screen/RegisterAkunScreen';
import LoginScreen from './screen/LoginScreen';
import SearchScreen from './screen/SearchScreen';
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyar_JNDkaRnBlmWnKos2t4xosHF-wKac&callback=initMap"></script>
import MerchantDetail from './screen/DetailMerchant';
import Tips1 from './screen/Tips1';
import Tips2 from './screen/Tips2';
import Tips3 from './screen/Tips3';
import Tips4 from './screen/Tips4';
import ListPesananScreen from './screen/ListPesananScreen';
import WaktuPemesananScreen from './screen/WaktuPemesananScreen';
import TambahMotorScreen from './screen/TambahMotorScreen';
import AturGarasimuScreen from './screen/AturGarasimuScreen';
import DashboardMerchantScreen from './screen/DashboardMerchantScreen';
import SplashScreen from './screen/SplashScreen';

const Stack = createNativeStackNavigator();
export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Splash" screenOptions={{headerShown:false}}>
        <Stack.Screen name="Splash" component={SplashScreen} />
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="RegisNav" component={RegisterAkunScreen} />
        <Stack.Screen name="RegisMerc" component={RegisterMerchantScreen} />
        <Stack.Screen name="RegisUser" component={RegisterPenggunaScreen} />
        <Stack.Screen name="Search" component={SearchScreen} />
        <Stack.Screen name="MercDetail" component={MerchantDetail} />
        <Stack.Screen name="LandingUser" component={LandingPageScreen} />
        <Stack.Screen name="Tips1" component={Tips1} />
        <Stack.Screen name="Tips2" component={Tips2} />
        <Stack.Screen name="Tips3" component={Tips3} />
        <Stack.Screen name="Tips4" component={Tips4} />
        <Stack.Screen name="ListPesanan" component={ListPesananScreen} />
        <Stack.Screen name="WaktuPemesanan" component={WaktuPemesananScreen} />
        <Stack.Screen name="TambahMotor" component={TambahMotorScreen} />
        <Stack.Screen name="AturGarasimu" component={AturGarasimuScreen} />
        <Stack.Screen name="DashboardMerchant" component={DashboardMerchantScreen} />
      </Stack.Navigator>
    </NavigationContainer>
    // <View style={styles.container}>
    // <StatusBar style="auto" />
    // <LandingPageScreen />
    // </View>

  );

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
