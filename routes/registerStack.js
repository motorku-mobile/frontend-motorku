import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import RegisterPenggunaScreen from "../screen/RegisterPenggunaScreen";
import RegisterMerchantScreen from "../screen/RegisterMerchantScreen";


const screens = {
    RegisterPenggunaScreen: {
        screen: RegisterPenggunaScreen
    },
    RegisterMerchantScreen: {
        screen: RegisterMerchantScreen
    },
    
}

const RegisterStack = createStackNavigator(screens);

export default createAppContainer(RegisterStack);